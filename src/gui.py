from selenium import webdriver
from selenium.webdriver.common.keys import Keys

browser = webdriver.Firefox()
theException = BaseException

while theException != BaseException :
   try:
      browser.get("http://www.michaelslab.net")
      search = browser.find_element_by_class_name('search-words')
      search.send_keys("Ada")
      submit = browser.find_element_by_class_name('search-submit')
      submit.click()

      search_result = browser.find_elements_by_class_name( "search-results")

      for i in search_result:
          print("element:", i, search_result[i]);

   except Exception as theException :
      print("**** exception ")
      print(type(theException), theException.args)
    